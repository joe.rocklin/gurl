package main

import (
	"flag"
	"fmt"
)

// Human-friendly help text. This should be alphabetical.
func printOptions(flagSet *flag.FlagSet) {
	fmt.Print("Help Options\n")

	fmt.Printf("      --cacert FILE          %s\n", flagSet.Lookup("cacert").Usage)
	fmt.Printf("  -d, --data DATA            %s\n", flagSet.Lookup("data").Usage)
	fmt.Printf("      --data-ascii DATA      %s\n", flagSet.Lookup("data-urlencode").Usage)
	fmt.Printf("      --data-urlencode DATA  %s\n", flagSet.Lookup("data-urlencode").Usage)
	fmt.Printf("  -h, --help                 %s\n", flagSet.Lookup("help").Usage)
	fmt.Printf("  -o, --output FILE          %s\n", flagSet.Lookup("output").Usage)
	fmt.Printf("  -X, --request COMMAND      %s\n", flagSet.Lookup("request").Usage)
	fmt.Printf("  -v, --version              %s\n", flagSet.Lookup("version").Usage)
	fmt.Printf("  -V, --verbose              %s\n", flagSet.Lookup("verbose").Usage)

	fmt.Printf("\n")
}
