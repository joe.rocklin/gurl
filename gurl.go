package main

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"runtime"
	"strings"

	"github.com/fatih/color"
)

var (
	// VERSION is initialized by the linker during compilation if the appropriate flag is specified:
	// e.g. go build -ldflags "-x main.VERSION 0.1.2-abcd" gurl.go
	// This idea pulled from goxc source
	VERSION = "1.0.1"

	// BUILD_DATE will be filled by goxc when it is compiled in the same manner as VERSION
	BUILD_DATE = ""

	// SOURCEDATE will be filled in by goxc interpolate-source
	SOURCEDATE = "2015-06-22T16:11:09-04:00"

	// Flags for execution adjustment
	cacert     string
	data       formData
	encodeData formDataURLEncode
	method     string
	outfile    string
	urls       []string
	verbose    bool
	isHelp     bool
	isVersion  bool
)

const (
	constMsgHelp = "Usage: gurl [flags] url [url]\n"
)

func gurl(call []string) error {
	interpretFlags(call)

	for _, url := range urls {
		fetch(url)
	}

	return nil
}

func printHelp(flagSet *flag.FlagSet) {
	args := flagSet.Args()
	if len(args) < 1 {
		printVersion(os.Stderr)
		fmt.Fprint(os.Stderr, "\n")
		printHelpTopic(flagSet, "options")
	}
}

func printHelpTopic(flagSet *flag.FlagSet, topic string) {
	switch topic {
	case "options":
		fmt.Fprint(os.Stderr, constMsgHelp)
		printOptions(flagSet)
		return
	}
}

func printVersion(output *os.File) {
	fmt.Fprintf(output, "gurl version: %s (%s/%s)\n", VERSION, runtime.GOOS, runtime.GOARCH)
	fmt.Fprintf(output, "  go version: %s, compiler: %s\n", runtime.Version(), runtime.Compiler)
	fmt.Fprintf(output, "  build date: %s\n", BUILD_DATE)
}

func interpretFlags(call []string) {
	var flagSet []*flag.FlagSet
	flagSet = append(flagSet, setupFlags())
	curFlags := flagSet[len(flagSet)-1]

	args := call

	// We have to loop until all args are parsed because cURL handles having URLs in the middle
	// of flags. This also requires us to set the default values on the flags (in setupFlags)
	// to the value of the variables so when setup gets called subsequent times it picks up the
	// values already set.
	for {
		err := curFlags.Parse(args[1:])
		if err != nil {
			log.Printf("Error parsing arguments: %s", err)
			os.Exit(1)
		}
		args = curFlags.Args()
		if len(args) == 0 {
			break
		}

		urls = append(urls, args[0])

		flagSet = append(flagSet, setupFlags())
		curFlags = flagSet[len(flagSet)-1]
	}

	if isHelp {
		printHelp(curFlags)
		os.Exit(0)
	}

	if isVersion {
		printVersion(os.Stdout)
		os.Exit(0)
	}

	// If no method has been defined, assume GET
	if method == "" {
		method = "GET"

		// If data params are provided, and the user has not forced a request method,
		// set the method to 'POST'
		if data.hasData() || encodeData.hasData() {
			method = "POST"
		}
	}

}

// Set up the flags - if you add a new flag here, also add the flag to the help-options disaplay in options.go
func setupFlags() *flag.FlagSet {
	flagSet := flag.NewFlagSet("gurl", flag.ContinueOnError)

	flagSet.BoolVar(&isHelp, "h", isHelp, "This help text")
	flagSet.BoolVar(&isHelp, "help", isHelp, "This help text")
	flagSet.BoolVar(&verbose, "v", verbose, "Make the operation more talkative")
	flagSet.BoolVar(&verbose, "verbose", verbose, "Make the operation more talkative")

	flagSet.StringVar(&cacert, "cacert", cacert, "CA certificate to verify peer against (SSL)")
	flagSet.StringVar(&outfile, "o", outfile, "Write output to <file> instead of stdout")
	flagSet.StringVar(&outfile, "output", outfile, "Write output to <file> instead of stdout")
	flagSet.StringVar(&method, "X", method, "Specify request command to use")
	flagSet.StringVar(&method, "request", method, "Specify request command to use")

	flagSet.Var(&data, "d", "HTTP POST data")
	flagSet.Var(&data, "data", "HTTP POST data")
	flagSet.Var(&data, "data-ascii", "HTTP POST ASCII data")
	flagSet.Var(&encodeData, "data-urlencode", "HTTP POST data url encoded")

	flagSet.BoolVar(&isVersion, "V", isVersion, "Show version number and quit")
	flagSet.BoolVar(&isVersion, "version", isVersion, "Show version number and quit")

	flagSet.Usage = func() {
		printHelp(flagSet)
	}

	return flagSet
}

func fetch(url string) (err error) {
	if verbose {
		log.Println("Fetching (" + method + ") " + url)
	}

	tr := &http.Transport{
		DisableCompression: true,
	}

	output := os.Stdout
	if outfile != "" {
		output, err = os.Create(outfile)
		if err != nil {
			color.Red("Failed to create ", outfile, " - ", err)
			return err
		}

		defer output.Close()
	}

	// Read a custom certificate chain to use
	if cacert != "" {
		if verbose {
			log.Printf("Using certificates from %s", cacert)
		}

		roots := x509.NewCertPool()
		cafile, err := readCACertFile(cacert)
		if err != nil {
			color.Red(" Failed to read CA File")
		}

		ok := roots.AppendCertsFromPEM(cafile)
		if !ok {
			color.Red(" Failed to parse rootPEM")
			return nil
		}
		tr.TLSClientConfig = &tls.Config{RootCAs: roots}
	}

	var body bytes.Buffer
	data.appendToBody(&body)
	encodeData.appendToBody(&body)

	req, err := http.NewRequest(method, url, strings.NewReader(body.String()))
	headerUA := fmt.Sprintf("gURL/%s (%s/%s)", VERSION, runtime.GOOS, runtime.GOARCH)
	req.Header.Set("User-Agent", headerUA)
	if data.hasData() || encodeData.hasData() {
		req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	}

	if verbose {
		printRequest(req)
	}

	client := &http.Client{Transport: tr}
	resp, err := client.Do(req)
	if err != nil {
		color.Red("Error doing URL request")
		fmt.Println(err)
		return err
	}
	defer resp.Body.Close()

	if verbose {
		printResponse(resp, false)
	}

	_, err = io.Copy(output, resp.Body)

	return err
}

func readCACertFile(cafile string) (file []byte, err error) {
	file, err = ioutil.ReadFile(cafile)
	// TODO: Do a sanity check on the file
	return file, err
}

func printRequest(req *http.Request) (err error) {
	body, err := httputil.DumpRequestOut(req, true)
	if err != nil {
		color.Red("Error getting request output")
		return err
	}
	fmt.Printf("----------- Request -----------\n%s\n--------------------------------\n", body)
	return err
}

func printResponse(resp *http.Response, printBody bool) (err error) {
	body, err := httputil.DumpResponse(resp, printBody)
	if err != nil {
		color.Red("Error getting response output")
		return err
	}
	fmt.Printf("----------- Response -----------\n%s--------------------------------\n", body)
	return err
}

func main() {
	log.SetPrefix("[gurl] ")

	err := gurl(os.Args)

	if err != nil {
		os.Exit(1)
	}
}
