package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"sort"
	"strings"

	"github.com/fatih/color"
)

type formData struct {
	data map[string][]string
}

type formDataURLEncode struct{ formData }

func (f *formData) String() string {
	return fmt.Sprint(f.data)
}

// Default ASCII formData processing - no encoding performed
func (f *formData) Set(input string) error {
	if f.data == nil {
		f.data = make(map[string][]string)
	}

	parts := strings.Split(input, "=")
	key := parts[0]
	value := parts[1]

	f.data[key] = append(f.data[key], value)
	return nil
}

// URLEncoded formData processing - Do URLEncoding
// This does not implement the _full_ cURL spec. We will accept forms of:
//   key=value
//   key@filename
// And perform the same operation that cURL's --data-urlencode would.
func (f *formDataURLEncode) Set(input string) error {
	// There are some extra checks to perform here in the case of the user requesting
	// to send a file
	var key, value string
	if strings.Index(input, "=") != -1 {
		parts := strings.Split(input, "=")
		key = parts[0]
		value = parts[1]
	} else if strings.Index(input, "@") != -1 {
		parts := strings.Split(input, "@")
		key = parts[0]
		filename := parts[1]
		if verbose {
			log.Println("URL Encoding ", filename)
		}

		file, err := ioutil.ReadFile(filename)
		if err != nil {
			color.Red("Unable to read %s", filename)
			os.Exit(-1)
		}
		value = string(file)
	}

	newInput := strings.Join([]string{key, url.QueryEscape(value)}, "=")
	return f.formData.Set(newInput)
}

func (f *formData) hasData() bool {
	if len(f.data) > 0 {
		return true
	}
	return false
}

// Add all of the data to the urlForm
func (f *formData) mergeURLForm(urlForm *url.Values) {
	for key, values := range f.data {
		for _, value := range values {
			//log.Printf("%s=%q\n", key, value)
			urlForm.Add(key, value)
		}
	}
}

func (f *formData) appendToBody(body *bytes.Buffer) {
	if f == nil || f.data == nil {
		return
	}

	keys := make([]string, 0, len(f.data))
	for k := range f.data {
		keys = append(keys, k)
	}

	sort.Strings(keys)

	for _, k := range keys {
		vs := f.data[k]
		prefix := url.QueryEscape(k) + "="
		for _, v := range vs {
			if body.Len() > 0 {
				body.WriteByte('&')
			}
			body.WriteString(prefix)
			body.WriteString(v)
		}
	}
}
