gurl
====

A simple cURL-like replacement, written in go.

The demand for this tool is to support older systems which, for various reasons, do not have updated version of OpenSSL and cURL and cannot communicate with the modern SSL server settings.

Currently supported protocols: HTTP, HTTPS

Build Prerequisites
-------------------

We're using [goxc](https://github.com/laher/goxc) for now to handle cross-compiling. This may change with go 1.5. After install `goxc`, run `goxc -t` to build all of the tool chains.

Build
-----

To build everything, run:

```
CGO_ENABLED=0 goxc -d build
```

Then look in the `build` directory for a directory which has all of the target binaries included.

Differences from cURL
=====================

1. Not everything is implemented
2. `-o` file output does not support the `#` syntax
3. `--data-urlencode` only supports the `name@filename` form
